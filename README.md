# GENERAL #

This repository will contain two shell scripts called 'sample-tweets.sh' and 'smileys.sh'. It also contains
a python script named 'get-gender.py', and a directory called 'names'.
The script 'sample-tweets.sh' accesses the in-house Twitter corpus. The shell script will access the twitter 
file from 1 March 2017 12:00 and analyze the tweets of this specific time:

- how many tweets are in the sample

- how many unique tweets are in the sample

- how many retweets are in the sample (out of the unique tweets)

In the end the script will show the first 20 unique tweets in the sample that are not retweets.

The script 'smileys.sh' is meant for my final project for this course. Just like the other shell script, it accesses the 
in-house Twitter corpus. It will analyze the amount of smileys used for both male and female twitter users over the years.
With the script, I want to investigate whether the use of smileys is more popular among females than males. 

The Python file 'get-gender.py' is used to determine whether a Tweet is made by a male of female user. To do this, it makes
use of the files in the 'names' directory, containing a large amount of both male and female names. 
This program is also used for my final project.

# USAGE #

'sample-tweets.sh': For each question, there is a path that goes to the actual tweets generated on 1 March 2017 12:00. 
Then, with a pipe, the twitter2-tool is used on these tweets. This returns the tweet with spaces inserted between words and punctuation.
Lastly, specific tweets are searched for with use of grep, and lines are counted with wc. This is done again by adding a pipe first. 

'smileys.sh': First, a path is made that goes directly to the collected Tweets. In this path, year and month in which you want to 
look for Tweets are specified. Then a new path, separated with a pipe first, is made which goes to the twitter2-tool. The tool
is applied on all the Tweets, and makes sure the text of the Tweets is printed. The -i option makes sure that the program will
ignore any errors. Lastly, again with a pipe, the 'smileys.sh' script is called. 
This script goes through the first 50.000 Tweets of each month, and counts how many male and female Tweets were generated. 
For this, it makes use of the 'get-gender.py' program. 
Lastly, it counts for both the male and female Tweets how many of them contain any smileys. It finally prints the amount of lines 
that contain smileys. 