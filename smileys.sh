#!/bin/bash
# Add the following line before calling this shell script, and put a pipe in between. You can change the month and year with with this line.
# zless /net/corpora/twitter2/Tweets/2016/01/*\:*.out.gz | /net/corpora/twitter2/tools/tweet2tab words | ./smileys.sh

# Here the variable 'path' is declared, which refers to the file get-gender.py
path=get-gender.py

echo "Amount of female tweets (out of the first 50000 lines in the file):"

head -n50000 | python $path | grep 'female' | wc -l

echo "Amount of male tweets (out of the first 50000 lines in the file):"

head -n50000 | python $path | grep 'male' | wc -l

echo "Amount of female tweets with smileys (out of the first 50000 lines in the file):"

head -n50000 | python $path | grep  'female' | grep -e ':)' -e ':-)' -e ':]' -e ':-]' -e ':>' -e ':->' -e '=)' -e '=)' -e ':}' -e ':-}' -e ':D' -e ':-D' -e ':3' -e '=3' -e ';)' -e ';-)' -e ';]' -e ';-]' -e ';3' -e ':(' -e ':-(' -e ':\[' -e ':-\[' -e ':<' -e ':-<' -e '=(' -e '=(' -e ':{' -e ':-{' -e 'D:' -e 'D-:' -e ';(' -e ';-(' -e ';\[' -e ';-\[' | wc -l

echo "Amount of male tweets with smileys (out of the first 50000 lines in the file):"

head -n50000 | python $path | grep -w 'male' | grep -e ':)' -e ':-)' -e ':]' -e ':-]' -e ':>' -e ':->' -e '=)' -e '=)' -e ':}' -e ':-}' -e ':D' -e ':-D' -e ':3' -e '=3' -e ';)' -e ';-)' -e ';]' -e ';-]' -e ';3' -e ':(' -e ':-(' -e ':\[' -e ':-\[' -e ':<' -e ':-<' -e '=(' -e '=(' -e ':{' -e ':-{' -e 'D:' -e 'D-:' -e ';(' -e ';-(' -e ';\[' -e ';-\[' | wc -l
