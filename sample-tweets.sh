#!/bin/bash

echo "How many tweets are in the sample?"

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab words | wc -l

echo "How many unique tweets are in the sample?"

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab words | uniq | wc -l

echo "How many retweets are in the sample (out of the unique tweets)?"

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab words | grep '^RT' |  wc -l

echo "What are the first 20 unique tweets in the sample that are not retweets?"

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab words | grep '^[^RT]' | uniq | head -20
